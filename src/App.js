import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';
import { cleanup } from '@testing-library/react';

import SockJS from 'sockjs-client';
import Stomp from 'stompjs'
function App() {

  function connectSocket(url) {
    return new Promise(function (resolve, reject) {
      const stomp = Stomp.over(new SockJS(url));
      stomp.debug = null;
      stomp.reconnect_delay = 5000;
      stomp.connect({},
        function () {
          resolve(stomp);
        },
        function (err) {
          reject(err);
        }
      );
    });
  }

  let raceId= 111;
  useEffect(() => {
    connectSocket(`http://localhost:8088/stomp`)
      .then(function (client: { subscribe: any }) {
        console.log('asdfhjsdfhlks connected');
        client.subscribe(`/topic/live-race/${raceId}`, function (message) {
          console.log('Received');
          let frameData = JSON.parse(message.body);
          console.log(frameData)
        });
      });
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
